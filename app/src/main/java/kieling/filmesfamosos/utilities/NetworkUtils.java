/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kieling.filmesfamosos.utilities;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.Scanner;

import kieling.filmesfamosos.BuildConfig;

/**
 * These utilities will be used to communicate with the network.
 */
public class NetworkUtils {
    public static final String POSTER_BASE_URL = "http://image.tmdb.org/t/p/w185";
    public static final String VIDEO_THUMBNAIL_BASE_URL = "https://img.youtube.com/vi/%s/0.jpg";
    private static final String API_BASE_URL = "https://api.themoviedb.org/3/movie";
    private static final String POSTFIX_URL_TOP_RATED = "/top_rated";
    private static final String POSTFIX_URL_UP_COMING = "/upcoming";
    private static final String POSTFIX_URL_DETAILS = "/%d";
    private static final String POSTFIX_URL_VIDEOS = "/%d/videos";
    private static final String POSTFIX_URL_REVIEWS = "/%d/reviews";
    private static final String PARAM_API_KEY = "api_key";
    private static final String TAG = "NetworkUtils";

    public static URL buildUrlTopRated() {
        return buildUrl(POSTFIX_URL_TOP_RATED);
    }

    public static URL buildUrlUpComing() {
        return buildUrl(POSTFIX_URL_UP_COMING);
    }

    public static URL buildUrlDetails(Integer movieId) {
        return buildUrl(String.format(Locale.getDefault(), POSTFIX_URL_DETAILS, movieId));
    }

    public static URL buildUrlVideos(Integer movieId) {
        return buildUrl(String.format(Locale.getDefault(), POSTFIX_URL_VIDEOS, movieId));
    }

    public static URL buildUrlReviews(Integer movieId) {
        return buildUrl(String.format(Locale.getDefault(), POSTFIX_URL_REVIEWS, movieId));
    }

    private static URL buildUrl(String postfixUrl) {
        Uri builtUri = Uri.parse(API_BASE_URL + postfixUrl).buildUpon()
                .appendQueryParameter(PARAM_API_KEY, BuildConfig.ApiKey)
                .build();

        URL url = null;
        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error building URL", e);
        }

        return url;
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }

    public static boolean checkOnlineState() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (Exception e) {
            Log.e(TAG, "Error checking connectivity", e);
        }
        return false;
    }
}