package kieling.filmesfamosos.ui.details;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import kieling.filmesfamosos.R;
import kieling.filmesfamosos.data.network.Video;
import kieling.filmesfamosos.utilities.NetworkUtils;

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> {
    private static final String TAG = "VideosAdapter";
    private final ListItemClickListener mOnClickListener;
    private List<Video> mDataSet;

    VideosAdapter(List<Video> dataSet, ListItemClickListener listener) {
        mDataSet = dataSet;
        mOnClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_video, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface ListItemClickListener {
        void onListMoviesItemClick(int clickedItemIndex);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView thumbnailView;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            thumbnailView = itemView.findViewById(R.id.itemVideoThumbnail);
        }

        void bind(int listIndex) {
            Video video = mDataSet.get(listIndex);
            Picasso .get()
                    .load(String.format(Locale.getDefault(), NetworkUtils.VIDEO_THUMBNAIL_BASE_URL, video.getKey()))
                    .into(thumbnailView);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            Log.d(TAG, "Video #" + clickedPosition + " clicked");
            mOnClickListener.onListMoviesItemClick(clickedPosition);
        }
    }
}
