package kieling.filmesfamosos.ui.details;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import kieling.filmesfamosos.R;
import kieling.filmesfamosos.data.network.Review;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {
    private List<Review> mDataSet;

    ReviewsAdapter(List<Review> dataSet) {
        mDataSet = dataSet;
    }

    @NonNull
    @Override
    public ReviewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_review, viewGroup, false);

        return new ReviewsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsAdapter.ViewHolder holder, int position) {
        Review review = mDataSet.get(position);
        holder.authorView.setText(review.getAuthor());
        holder.contentView.setText(review.getContent());
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView authorView;
        TextView contentView;

        ViewHolder(View itemView) {
            super(itemView);
            authorView = itemView.findViewById(R.id.itemReviewAuthor);
            contentView = itemView.findViewById(R.id.itemReviewContent);
        }
    }
}
