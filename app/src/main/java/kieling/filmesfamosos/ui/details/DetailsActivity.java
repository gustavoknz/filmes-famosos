package kieling.filmesfamosos.ui.details;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;

import kieling.filmesfamosos.R;
import kieling.filmesfamosos.data.network.ResponseDetails;
import kieling.filmesfamosos.data.network.ResponseMovies;
import kieling.filmesfamosos.data.network.ResponseReviews;
import kieling.filmesfamosos.data.network.ResponseVideos;
import kieling.filmesfamosos.data.network.Review;
import kieling.filmesfamosos.data.network.Video;
import kieling.filmesfamosos.database.MovieEntry;
import kieling.filmesfamosos.database.MoviesDatabase;
import kieling.filmesfamosos.utilities.FfConstants;
import kieling.filmesfamosos.utilities.NetworkUtils;

public class DetailsActivity extends AppCompatActivity implements VideosAdapter.ListItemClickListener,
        LoaderManager.LoaderCallbacks<Object> {
    private static final String TAG = "DetailsActivity";
    private static final int API_SEARCH_DETAILS_LOADER = 43;
    private static final int API_VIDEOS_LOADER = 44;
    private static final int API_REVIEWS_LOADER = 45;
    private static final String APP_STATE_KEY = "appState";
    private static final String MOVIE_ID_KEY = "movieId";
    private boolean mIsFavorite;
    private boolean mVideoLoaderHasFinished;
    private boolean mReviewLoaderHasFinished;
    private String mAppState;
    private ResponseDetails mMovieDetails;
    private ProgressBar mProgressBar;
    private List<Video> mListVideos;
    private List<Review> mListReviews;

    private static void watchYoutubeVideo(Context context, String key) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + key));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + key));
            context.startActivity(webIntent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        mVideoLoaderHasFinished = false;
        mReviewLoaderHasFinished = false;
        mProgressBar = findViewById(R.id.detailsProgressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        MoviesDatabase.getInstance(getApplicationContext())
                .movieDao()
                .count()
                .observe(this, count -> Log.d(TAG, "total count: " + count)
                );

        if (mListVideos == null) {
            mListVideos = new ArrayList<>();
        }
        if (mListReviews == null) {
            mListReviews = new ArrayList<>();
        }

        if (savedInstanceState != null) {
            mAppState = savedInstanceState.getString(APP_STATE_KEY);
            populateDetails();
            populateVideos();
            return;
        }
        Intent intent = getIntent();
        int movieId = intent.getIntExtra(FfConstants.PARAM_MOVIE_ID_KEY, -1);
        Log.d(TAG, "Showing details for movie id = " + movieId);
        if (movieId == -1) {
            Log.w(TAG, "Error on retrieving movieId");
        } else if (NetworkUtils.checkOnlineState()) {
            Bundle args = new Bundle();
            args.putInt(MOVIE_ID_KEY, movieId);
            LoaderManager loaderManager = getSupportLoaderManager();

            // Search details loader
            Loader<ResponseMovies> apiSearchLoader = loaderManager.getLoader(API_SEARCH_DETAILS_LOADER);
            if (apiSearchLoader == null) {
                Log.d(TAG, "Initializing details loader");
                loaderManager.initLoader(API_SEARCH_DETAILS_LOADER, args, this);
            } else {
                Log.d(TAG, "Restarting details loader");
                loaderManager.restartLoader(API_SEARCH_DETAILS_LOADER, args, this);
            }

            // Videos loader
            Loader<ResponseVideos> apiVideosLoader = loaderManager.getLoader(API_VIDEOS_LOADER);
            if (apiVideosLoader == null) {
                Log.d(TAG, "Initializing videos loader");
                loaderManager.initLoader(API_VIDEOS_LOADER, args, this);
            } else {
                Log.d(TAG, "Restarting videos loader");
                loaderManager.restartLoader(API_VIDEOS_LOADER, args, this);
            }

            // Comments loader
            Loader<ResponseReviews> apiReviewsLoader = loaderManager.getLoader(API_REVIEWS_LOADER);
            if (apiReviewsLoader == null) {
                Log.d(TAG, "Initializing reviews loader");
                loaderManager.initLoader(API_REVIEWS_LOADER, args, this);
            } else {
                Log.d(TAG, "Restarting reviews loader");
                loaderManager.restartLoader(API_REVIEWS_LOADER, args, this);
            }
        } else {
            Log.d(TAG, "Cannot do much offline!");
            TextView textNoConnection = findViewById(R.id.detailsNoConnection);
            textNoConnection.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void populateDetails() {
        TextView titleView = findViewById(R.id.detailsTitle);
        ImageView backdropView = findViewById(R.id.detailsBackdrop);
        ImageView pictureView = findViewById(R.id.detailsPoster);
        TextView overviewView = findViewById(R.id.detailsOverview);
        TextView dateView = findViewById(R.id.detailsDate);
        TextView gradeView = findViewById(R.id.detailsGrade);
        titleView.setText(mMovieDetails.getTitle());
        Picasso.get().load(NetworkUtils.POSTER_BASE_URL + mMovieDetails.getBackdropPath()).into(backdropView);
        Picasso.get().load(NetworkUtils.POSTER_BASE_URL + mMovieDetails.getPosterPath()).into(pictureView);
        overviewView.setText(mMovieDetails.getOverview());
        dateView.setText(mMovieDetails.getReleaseDate());
        gradeView.setText(String.format(Locale.getDefault(), getString(R.string.details_grade), mMovieDetails.getVoteAverage()));
    }

    private void populateVideos() {
        Log.d(TAG, "Creating a new video list");
        RecyclerView listVideosView = findViewById(R.id.detailsVideos);
        LinearLayoutManager layoutManagerVideos = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        listVideosView.setLayoutManager(layoutManagerVideos);
        listVideosView.setHasFixedSize(true);
        listVideosView.setAdapter(new VideosAdapter(mListVideos, this));
    }

    private void populateReviews() {
        Log.d(TAG, "Creating a new review list");
        RecyclerView listReviewsView = findViewById(R.id.detailsReviews);
        listReviewsView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        listReviewsView.setHasFixedSize(true);
        listReviewsView.setAdapter(new ReviewsAdapter(mListReviews));
    }

    @Override
    public void onListMoviesItemClick(int clickedItemIndex) {
        Video video = mListVideos.get(clickedItemIndex);
        Log.d(TAG, " video: " + video.getName());
        watchYoutubeVideo(this, video.getKey());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(APP_STATE_KEY, mAppState);

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        mAppState = state.getString(APP_STATE_KEY);
    }

    @NonNull
    @Override
    public Loader<Object> onCreateLoader(int id, @Nullable final Bundle args) {
        if (id == API_SEARCH_DETAILS_LOADER) {
            return new DetailsAsyncTaskLoader(this, args);
        } else if (id == API_VIDEOS_LOADER) {
            return new VideosAsyncTaskLoader(this, args);
        } else if (id == API_REVIEWS_LOADER) {
            return new ReviewsAsyncTaskLoader(this, args);
        } else {
            Log.d(TAG, "Unknown loader id: " + id);
            return new DummyAsyncTaskLoader(this);
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader loader, Object data) {
        if (loader.getId() == API_SEARCH_DETAILS_LOADER) {
            Log.d(TAG, "Details " + data + " items");
            mMovieDetails = (ResponseDetails) data;
            populateDetails();

            final ImageView starView = findViewById(R.id.detailsFavoriteImageView);
            starView.setVisibility(View.VISIBLE);
            MoviesDatabase.getInstance(getApplicationContext())
                    .movieDao()
                    .getFavorite(mMovieDetails.getServerId())
                    .observe(this, favorite -> {
                                Log.d(TAG, "Found isFavorite: " + favorite);
                                mIsFavorite = Integer.valueOf(1).equals(favorite);
                                if (mIsFavorite) {
                                    starView.setImageResource(android.R.drawable.star_big_on);
                                } else {
                                    starView.setImageResource(android.R.drawable.star_big_off);
                                }
                            }
                    );
        } else if (loader.getId() == API_VIDEOS_LOADER) {
            Log.d(TAG, "Videos " + data + " items");
            ResponseVideos movieVideos = (ResponseVideos) data;
            mListVideos = movieVideos.getVideos();
            populateVideos();
            mVideoLoaderHasFinished = true;
            if (mReviewLoaderHasFinished) {
                mProgressBar.setVisibility(View.GONE);
            }
        } else if (loader.getId() == API_REVIEWS_LOADER) {
            Log.d(TAG, "Reviews " + data + " items");
            ResponseReviews movieReviews = (ResponseReviews) data;
            mListReviews = movieReviews.getReviews();
            populateReviews();
            mReviewLoaderHasFinished = true;
            if (mVideoLoaderHasFinished) {
                mProgressBar.setVisibility(View.GONE);
            }
        } else {
            Log.i(TAG, "Loader not handled");
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader loader) {
        mProgressBar.setVisibility(View.GONE);
    }

    public void starClicked(View view) {
        //TODO: Pq tenho q trocar o valor aqui em vez de ser atualizado no observe do getFavorite?
        mIsFavorite = !mIsFavorite;
        Log.d(TAG, "Star favorite clicked. Favorite? " + mIsFavorite);
        if (mIsFavorite) {
            Executors.newSingleThreadExecutor().execute(() -> MoviesDatabase.getInstance(getApplicationContext())
                    .movieDao()
                    .insert(buildMovie()));
        } else {
            Executors.newSingleThreadExecutor().execute(() -> MoviesDatabase.getInstance(getApplicationContext())
                    .movieDao()
                    .delete(mMovieDetails.getServerId()));
        }
    }

    private MovieEntry buildMovie() {
        return new MovieEntry(mMovieDetails.getServerId(), mMovieDetails.getTitle(),
                mMovieDetails.getBackdropPath(), mMovieDetails.getPosterPath(),
                mMovieDetails.getOverview(), mMovieDetails.getReleaseDate(),
                mMovieDetails.getVoteAverage(), mIsFavorite ? 1 : 0);
    }

    private static class DetailsAsyncTaskLoader extends AsyncTaskLoader<Object> {
        private final Bundle mArgs;

        DetailsAsyncTaskLoader(@NonNull Context context, Bundle args) {
            super(context);
            this.mArgs = args;
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Nullable
        @Override
        public ResponseDetails loadInBackground() {
            int movieId = mArgs.getInt(MOVIE_ID_KEY);
            String moviesResults;
            URL url = NetworkUtils.buildUrlDetails(movieId);
            Log.d(TAG, "Fetching details on " + url);
            try {
                moviesResults = NetworkUtils.getResponseFromHttpUrl(url);
                return new Gson().fromJson(moviesResults, ResponseDetails.class);
            } catch (IOException e) {
                Log.e(TAG, "IO details error", e);
            }
            return new ResponseDetails();
        }
    }

    private static class VideosAsyncTaskLoader extends AsyncTaskLoader<Object> {
        private final Bundle mArgs;

        VideosAsyncTaskLoader(@NonNull Context context, Bundle args) {
            super(context);
            this.mArgs = args;
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Nullable
        @Override
        public ResponseVideos loadInBackground() {
            int movieId = mArgs.getInt(MOVIE_ID_KEY);
            String videosResults;
            URL url = NetworkUtils.buildUrlVideos(movieId);
            Log.d(TAG, "Fetching videos on " + url);
            try {
                videosResults = NetworkUtils.getResponseFromHttpUrl(url);
                return new Gson().fromJson(videosResults, ResponseVideos.class);
            } catch (IOException e) {
                Log.e(TAG, "IO videos error", e);
            }
            return new ResponseVideos();
        }
    }

    private static class ReviewsAsyncTaskLoader extends AsyncTaskLoader<Object> {
        private final Bundle mArgs;

        ReviewsAsyncTaskLoader(@NonNull Context context, Bundle args) {
            super(context);
            this.mArgs = args;
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Nullable
        @Override
        public ResponseReviews loadInBackground() {
            int movieId = mArgs.getInt(MOVIE_ID_KEY);
            String reviewsResults;
            URL url = NetworkUtils.buildUrlReviews(movieId);
            Log.d(TAG, "Fetching reviews on " + url);
            try {
                reviewsResults = NetworkUtils.getResponseFromHttpUrl(url);
                return new Gson().fromJson(reviewsResults, ResponseReviews.class);
            } catch (IOException e) {
                Log.e(TAG, "IO reviews error", e);
            }
            return new ResponseReviews();
        }
    }

    private static class DummyAsyncTaskLoader extends AsyncTaskLoader<Object> {
        DummyAsyncTaskLoader(@NonNull Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Nullable
        @Override
        public Object loadInBackground() {
            return new ResponseDetails();
        }
    }
}
