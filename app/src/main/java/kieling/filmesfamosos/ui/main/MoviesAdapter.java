package kieling.filmesfamosos.ui.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import kieling.filmesfamosos.R;
import kieling.filmesfamosos.data.network.Movie;
import kieling.filmesfamosos.utilities.NetworkUtils;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {
    private final ListItemClickListener mOnClickListener;
    private List<Movie> mDataSet;

    MoviesAdapter(List<Movie> dataSet, ListItemClickListener listener) {
        mDataSet = dataSet;
        mOnClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_movie, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView favoriteIcon;
        ImageView posterImage;
        TextView titleText;

        ViewHolder(View itemView) {
            super(itemView);
            favoriteIcon = itemView.findViewById(R.id.itemMovieFavorite);
            posterImage = itemView.findViewById(R.id.itemMoviePicture);
            titleText = itemView.findViewById(R.id.itemMovieTitle);
            itemView.setOnClickListener(this);
        }

        void bind(int listIndex) {
            Movie movie = mDataSet.get(listIndex);
            if (Integer.valueOf(1).equals(movie.getIsFavorite())) {
                favoriteIcon.setImageResource(android.R.drawable.star_on);
                favoriteIcon.setContentDescription("This is a favorite movie");
            } else {
                favoriteIcon.setImageResource(android.R.drawable.star_off);
                favoriteIcon.setContentDescription("This is a not favorite movie");
            }
            Picasso.get().load(NetworkUtils.POSTER_BASE_URL + movie.getPosterPath()).into(posterImage);
            titleText.setText(movie.getTitle());
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition);
        }
    }
}
