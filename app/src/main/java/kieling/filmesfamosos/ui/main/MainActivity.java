package kieling.filmesfamosos.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import kieling.filmesfamosos.R;
import kieling.filmesfamosos.data.network.Movie;
import kieling.filmesfamosos.data.network.ResponseMovies;
import kieling.filmesfamosos.database.MovieEntry;
import kieling.filmesfamosos.database.MoviesDatabase;
import kieling.filmesfamosos.ui.details.DetailsActivity;
import kieling.filmesfamosos.utilities.FfConstants;
import kieling.filmesfamosos.utilities.NetworkUtils;

//TODO: chegando na penultima linha, carregar mais dados
//TODO: mostrar erro quando sem internet
public class MainActivity extends AppCompatActivity implements MoviesAdapter.ListItemClickListener,
        LoaderManager.LoaderCallbacks<ResponseMovies> {
    private static final int API_SEARCH_LOADER = 42;
    private static final String TAG = "MainActivity";
    private static final String LIST_STATE_KEY = "listStateKey";
    //TODO: usar flags para controlar ciclo de vida da activity eh feio
    private static boolean sShouldExecuteMovieTopRatedAsyncTaskLoader;
    private int mCurrentMenu;
    private TextView mTextNoConnection;
    private ProgressBar mProgressBar;
    private RecyclerView.Adapter mAdapter;
    private List<Movie> mListItems;
    private LinearLayoutManager mLayoutManager;
    private Parcelable mListState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "(onCreate)");
        mTextNoConnection = findViewById(R.id.mainNoConnection);
        mProgressBar = findViewById(R.id.mainProgressBar);
        mProgressBar.setVisibility(View.VISIBLE);

        RecyclerView listMoviesView = findViewById(R.id.mainListMovies);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        listMoviesView.setLayoutManager(mLayoutManager);
        listMoviesView.setHasFixedSize(true);
        sShouldExecuteMovieTopRatedAsyncTaskLoader = false;
        if (savedInstanceState == null) {
            Log.d(TAG, "Creating a new list");
            mListItems = new ArrayList<>();
            touchLoader();
        } else {
            Log.d(TAG, "NOT creating a new list");
            mProgressBar.setVisibility(View.VISIBLE);
        }
        mAdapter = new MoviesAdapter(mListItems, this);
        listMoviesView.setAdapter(mAdapter);
    }

    private void touchLoader() {
        LoaderManager loaderManager = getSupportLoaderManager();
        Loader<ResponseMovies> apiSearchLoader = loaderManager.getLoader(API_SEARCH_LOADER);
        if (apiSearchLoader == null) {
            Log.d(TAG, "Initializing loader");
            loaderManager.initLoader(API_SEARCH_LOADER, null, this);
        } else {
            Log.d(TAG, "Restarting loader");
            loaderManager.restartLoader(API_SEARCH_LOADER, null, this);
        }
    }

    private void getAllFavoriteMovies() {
        Log.d(TAG, "Searching for favourite movies...");

        Executors.newSingleThreadExecutor().execute(() -> {
            List<MovieEntry> entries = MoviesDatabase.getInstance(getApplicationContext())
                    .movieDao()
                    .getFavorites();
            mListItems.clear();
            for (MovieEntry entry : entries) {
                mListItems.add(new Movie(entry.getServerId(), entry.getTitle(),
                        entry.getBackdropPath(), entry.getPictureUrl(), entry.getIsFavorite()));
            }
            runOnUiThread(() -> mAdapter.notifyDataSetChanged());
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "(onResume) mListState: " + mListState);
        if (mListState != null) {
            mLayoutManager.onRestoreInstanceState(mListState);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        // Save list state
        Log.d(TAG, "(onSaveInstanceState) mListState: " + mListState);
        mListState = mLayoutManager.onSaveInstanceState();
        Log.d(TAG, "(onSaveInstanceState) mListState: " + mListState);
        state.putParcelable(LIST_STATE_KEY, mListState);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);

        // Retrieve list state and list/item positions
        Log.d(TAG, "(onRestoreInstanceState) mListState: " + mListState);
        if (state != null) {
            mListState = state.getParcelable(LIST_STATE_KEY);
        }
        Log.d(TAG, "(onRestoreInstanceState) mListState: " + mListState);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionTopRated:
                if (mCurrentMenu != R.id.actionTopRated) {
                    Log.d(TAG, "Top rated tapped");
                    mCurrentMenu = R.id.actionTopRated;
                    touchLoader();
                }
                return true;
            case R.id.actionUpComing:
                if (mCurrentMenu != R.id.actionUpComing) {
                    Log.d(TAG, "Up coming tapped");
                    mCurrentMenu = R.id.actionUpComing;
                    touchLoader();
                }
                return true;
            case R.id.actionFavorites:
                if (mCurrentMenu != R.id.actionFavorites) {
                    Log.d(TAG, "Favorites tapped");
                    mCurrentMenu = R.id.actionFavorites;
                    getAllFavoriteMovies();
                    mProgressBar.setVisibility(View.GONE);
                }
                return true;
            default:
                Log.d(TAG, "Tapped " + item.getItemId());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
        Log.d(TAG, "Item #" + clickedItemIndex + " clicked");
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(FfConstants.PARAM_MOVIE_ID_KEY, mListItems.get(clickedItemIndex).getId());
        startActivity(intent);
    }

    @NonNull
    @Override
    public Loader<ResponseMovies> onCreateLoader(int id, @Nullable Bundle args) {
        if (NetworkUtils.checkOnlineState()) {
            mProgressBar.setVisibility(View.VISIBLE);
            if (mCurrentMenu == R.id.actionUpComing) {
                Log.d(TAG, "(onCreateLoader) Searching up coming...");
                return new MovieUpComingAsyncTaskLoader(getApplicationContext());
            } else if (mCurrentMenu == R.id.actionTopRated) {
                Log.d(TAG, "(onCreateLoader) Searching top rated...");
                sShouldExecuteMovieTopRatedAsyncTaskLoader = true;
                return new MovieTopRatedAsyncTaskLoader(getApplicationContext());
            } else {
                Log.d(TAG, "(onCreateLoader) Searching for the first time? " + mCurrentMenu);
                sShouldExecuteMovieTopRatedAsyncTaskLoader = true;
                return new MovieTopRatedAsyncTaskLoader(getApplicationContext());
            }
        } else {
            Log.d(TAG, "Cannot do much offline");
            mTextNoConnection.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
            return new DummyAsyncTaskLoader(getApplicationContext());
        }
    }

    @Override
    public void onLoadFinished(@NonNull Loader<ResponseMovies> loader, ResponseMovies data) {
        if (data == null) {
            Log.e(TAG, "Error loading data");
        } else if (mCurrentMenu == R.id.actionFavorites) {
            Log.d(TAG, "Should NOT execute *AsyncTaskLoader");
        } else {
            sShouldExecuteMovieTopRatedAsyncTaskLoader = false;
            List<Movie> movies = data.getMovies();
            Log.d(TAG, "Downloaded " + movies.size() + " items");
            mListItems.clear();
            mListItems.addAll(movies);
            mAdapter.notifyDataSetChanged();

            //Retrieve favorite info
            Executors.newSingleThreadExecutor().execute(() -> {
                List<Integer> entryServerIds = MoviesDatabase.getInstance(getApplicationContext())
                        .movieDao()
                        .getFavoritesId();
                for (Integer entryServerId : entryServerIds) {
                    for (Movie movie : mListItems) {
                        if (entryServerId.equals(movie.getId())) {
                            Log.d(TAG, "Found favorite: " + movie.getId() + "; " + movie.getTitle());
                            movie.setIsFavorite(1);
                            break;
                        }
                    }
                }
            });
        }
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<ResponseMovies> loader) {
        mProgressBar.setVisibility(View.GONE);
    }

    private static class MovieUpComingAsyncTaskLoader extends AsyncTaskLoader<ResponseMovies> {
        MovieUpComingAsyncTaskLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Nullable
        @Override
        public ResponseMovies loadInBackground() {
            String moviesResults;
            Log.d(TAG, "(MovieUpComingAsyncTaskLoader) Searching up coming...");
            URL url = NetworkUtils.buildUrlUpComing();
            try {
                moviesResults = NetworkUtils.getResponseFromHttpUrl(url);
                return new Gson().fromJson(moviesResults, ResponseMovies.class);
            } catch (IOException e) {
                Log.e(TAG, "IO error", e);
            }
            return new ResponseMovies();
        }
    }

    private static class MovieTopRatedAsyncTaskLoader extends AsyncTaskLoader<ResponseMovies> {
        MovieTopRatedAsyncTaskLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            Log.d(TAG, "sShouldExecuteMovieTopRatedAsyncTaskLoader? " + sShouldExecuteMovieTopRatedAsyncTaskLoader);
            if (sShouldExecuteMovieTopRatedAsyncTaskLoader) {
                forceLoad();
            }
        }

        @Nullable
        @Override
        public ResponseMovies loadInBackground() {
            String moviesResults;
            Log.d(TAG, "(MovieTopRatedAsyncTaskLoader) Searching top rated...");
            URL url = NetworkUtils.buildUrlTopRated();
            try {
                moviesResults = NetworkUtils.getResponseFromHttpUrl(url);
                return new Gson().fromJson(moviesResults, ResponseMovies.class);
            } catch (IOException e) {
                Log.e(TAG, "IO error", e);
            }

            return new ResponseMovies();
        }
    }

    private static class DummyAsyncTaskLoader extends AsyncTaskLoader<ResponseMovies> {
        DummyAsyncTaskLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Nullable
        @Override
        public ResponseMovies loadInBackground() {
            return new ResponseMovies();
        }
    }
}
