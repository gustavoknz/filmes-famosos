package kieling.filmesfamosos.data.network;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResponseVideos {
    @SerializedName("results")
    private List<Video> videos;

    public List<Video> getVideos() {
        return videos == null ? new ArrayList<Video>() : videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    @Override
    public String toString() {
        return "ResponseVideos{" +
                "videos=" + videos +
                '}';
    }
}
