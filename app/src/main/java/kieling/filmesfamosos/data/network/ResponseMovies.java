package kieling.filmesfamosos.data.network;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResponseMovies {
    @SerializedName("results")
    private List<Movie> movies;

    public List<Movie> getMovies() {
        return movies == null ? new ArrayList<>() : movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public String toString() {
        return "ResponseMovies{" +
                "movies=" + movies +
                '}';
    }
}
