package kieling.filmesfamosos.data.network;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResponseReviews {
    @SerializedName("results")
    private List<Review> reviews;

    public List<Review> getReviews() {
        return reviews == null ? new ArrayList<Review>() : reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public String toString() {
        return "ResponseReviews{" +
                "reviews=" + reviews +
                '}';
    }
}
