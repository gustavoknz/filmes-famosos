package kieling.filmesfamosos.data.network;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Video implements Parcelable {
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    @SerializedName("key")
    private String key;
    @SerializedName("name")
    private String name;
    @SerializedName("poster_path")
    private String site;

    // Parcelling part
    public Video(Parcel in) {
        this.key = in.readString();
        this.name = in.readString();
        this.site = in.readString();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key);
        dest.writeString(this.name);
        dest.writeString(this.site);
    }

    @Override
    public String toString() {
        return "Video{" +
                "key=" + key +
                ", name='" + name + '\'' +
                ", site='" + site + '\'' +
                '}';
    }
}
