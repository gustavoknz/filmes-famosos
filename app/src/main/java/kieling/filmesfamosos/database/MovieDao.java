/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kieling.filmesfamosos.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import kieling.filmesfamosos.data.network.Movie;

/**
 * {@link Dao} which provides an api for all data operations with the {@link MoviesDatabase}
 */
@Dao
public interface MovieDao {
    /**
     * Gets all movies
     *
     * @return {@link LiveData} with all movies
     */
    @Query("SELECT * FROM movie")
    LiveData<MovieEntry> getAll();

    @Query("SELECT count(*) FROM movie")
    LiveData<Integer> count();

    @Query("SELECT * FROM movie")
    List<MovieEntry> getFavorites();

    @Query("SELECT serverId FROM movie")
    List<Integer> getFavoritesId();

    @Query("SELECT isFavorite FROM movie WHERE serverId = :serverId")
    LiveData<Integer> getFavorite(int serverId);

    /**
     * Inserts a {@link MovieEntry} into the movie table. If there is a conflicting id the movie
     * entry uses the {@link OnConflictStrategy} of replacing the movie.
     * The required uniqueness of these values is defined in the {@link MovieEntry}.
     *
     * @param movie A movie to insert
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MovieEntry movie);

    @Query("DELETE FROM movie WHERE serverId = :serverId")
    void delete(int serverId);
}
